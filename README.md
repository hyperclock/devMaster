## devMaster

A highly modded version of Remastersys 3.0.0-1 designed to work with SystemD.
SysV may work, but will not be of much concern for the Cybernux OS project. 

I suggest testing this and reporting bugs here, they may get fixed at some point.


### License Details

The original tool, Remastersys, was released under GPLv2.

I will upgrade the licence for this tool to GPLv3.

_See [LICENSE](LICENSE) for a detailed copy._


### Tool's Purpose

**Producing a distributable CD/DVD, _THE REMASTERSYS WAY_,  without the bloat.**


### WIKI

is on the way...